export VI_MODE_SET_CURSOR=true

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
    source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

zstyle ':completion:*' menu select
zstyle ':completion:*' rehash true
zmodload zsh/complist

# use the vi navigation keys in menu completion
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

SAVEHIST=10000
HISTSIZE=10000
HISTFILE=~/.zsh_history

setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_SAVE_NO_DUPS
setopt HIST_VERIFY
setopt SHARE_HISTORY

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
export EDITOR='nvim'

bindkey -v

export KEYTIMEOUT=1

ZLE_RPROMPT_INDENT=0

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=240"
ZSH_AUTOSUGGEST_STRATEGY=(history completion)

alias ls="exa -al --icons --group-directories-first"
alias nv="neovide --multigrid"

HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE=1

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)

bgnotify_threshold=5
function bgnotify_formatted { ## args: (exit_status, command, elapsed_seconds)
    elapsed="$(( $3 % 60 ))s"
    (( $3 >= 60 )) && elapsed="$((( $3 % 3600) / 60 ))m $elapsed"
    (( $3 >= 3600 )) && elapsed="$(( $3 / 3600 ))h $elapsed"
    [ $1 -eq 0 ] && bgnotify "✓ (took $elapsed)" "$2" || bgnotify "✘ (took $elapsed)" "$2"
}
export DIFFPROG="nvim -d"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

alias unissh="ssh jpr539@uoyssh"

source <(antibody init)

antibody bundle jeffreytse/zsh-vi-mode
antibody bundle t413/zsh-background-notify
antibody bundle zsh-users/zsh-autosuggestions
antibody bundle zdharma-continuum/fast-syntax-highlighting
antibody bundle zsh-users/zsh-history-substring-search

antibody bundle romkatv/powerlevel10k

bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
bindkey -M viins '^[[A' history-substring-search-up
bindkey -M viins '^[OA' history-substring-search-up
bindkey -M viins '^[[B' history-substring-search-down
bindkey -M viins '^[OB' history-substring-search-down

export fpath=(~/.zsh/functions $fpath)

autoload -Uz compinit
typeset -i updated_at=$(date +'%j' -r ~/.zcompdump 2>/dev/null || stat -f '%Sm' -t '%j' ~/.zcompdump 2>/dev/null)
if [ $(date +'%j') != $updated_at ]; then
  compinit -i
else
  compinit -C -i
fi

export GHCUP_USE_XDG_DIRS=1

eval "$(zoxide init zsh)"
source /usr/share/doc/pkgfile/command-not-found.zsh
