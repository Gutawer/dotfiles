#!/usr/bin/env lua

local function trim(s)
    return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end

local ew = io.popen "i3-msg -t get_workspaces"
local workspaces_json = ew:read "*a"
ew:close()
local workspaces = require("cjson").decode(workspaces_json)

local box = '(box :orientation "h" :vexpand true :spacing 0 :space-evenly "false" '

for _, w in pairs(workspaces) do
    local cls = ""
    if w.urgent then
        cls = "urgent"
    elseif w.visible then
        cls = "active"
    else
        cls = "inactive"
    end
    local btn = '(eventbox :cursor "pointer" (button :class "'
        .. cls
        .. '" :hexpand "true" :onclick "i3-msg \'workspace '
        .. w.name
        .. '\'" "'
        .. w.name
        .. '"))'
    box = box .. btn
end

box = box .. ")"

print(box)
