#!/bin/sh
pamixer --get-mute
pactl subscribe | grep --line-buffered "sink" | while read -r; do
    pamixer --get-mute
done
