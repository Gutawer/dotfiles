#!/usr/bin/env lua

local function trim(s)
    return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end

local aw =
    io.popen "hyprctl monitors | grep active | sed 's/()/(1)/g' | sort | awk 'NR>1{print $1}' RS='(' FS=')'"
local active_workspace = aw:read "*a"
aw:close()

local ew =
    io.popen "hyprctl workspaces | grep ID | sed 's/()/(1)/g' | sort | awk 'NR>1{print $1}' RS='(' FS=')'"
local existing_workspaces = ew:read "*a"
ew:close()

local box = '(box :orientation "h" :vexpand true :spacing 0 :space-evenly "false" '

for i = 1, #existing_workspaces do
    local c = existing_workspaces:sub(i, i)
    if tonumber(c) == tonumber(active_workspace) then
        local btn = '(eventbox :cursor "pointer" (button :class "active" :hexpand "true" :onclick "hyprctl dispatch workspace '
            .. c
            .. ' " "'
            .. c
            .. '"))'
        box = box .. btn
    elseif c ~= "\n" then
        local btn = '(eventbox :cursor "pointer" (button :class "inactive" :hexpand "true" :onclick "hyprctl dispatch workspace '
            .. c
            .. ' " "'
            .. c
            .. '"))'
        box = box .. btn
    end
end

box = box .. ")"

print(box)
