#!/usr/bin/python3

import subprocess

PLAYER = "Spot"

def main():
    base = ["playerctl", "-p", PLAYER, "metadata", "-f"]
    artist_cmd = base + ["{{artist}}"]
    title_cmd = base + ["{{title}}"]

    artist = subprocess.run(artist_cmd, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    if artist.returncode != 0:
        return
    artist = artist.stdout.decode("utf-8").rstrip()

    title = subprocess.run(title_cmd, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    if title.returncode != 0:
        return
    title = title.stdout.decode("utf-8").rstrip()

    if title == "Not playing":
        return

    print(f" {artist} - {title}")

if __name__ == '__main__':
    main()
