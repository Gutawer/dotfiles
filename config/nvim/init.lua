_G.cfg_globals = {
    signs = { Error = "", Warn = "", Hint = "", Info = "" },
    telescope = function(picker)
        return string.format(
            '<cmd> lua require("telescope.builtin").%s(require("telescope.themes").get_dropdown())<CR>',
            picker
        )
    end,
    map = vim.api.nvim_set_keymap,
}

local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    if not vim.loop.fs_stat(lazypath) then
        vim.fn.system {
            "git",
            "clone",
            "--filter=blob:none",
            "https://github.com/folke/lazy.nvim.git",
            lazypath,
        }
        vim.fn.system { "git", "-C", lazypath, "checkout", "tags/stable" } -- last stable release
    end
end
vim.opt.rtp:prepend(lazypath)
require("lazy").setup "plugins"

vim.opt.compatible = false
vim.opt.termguicolors = true

vim.opt.comments:prepend { ":///" }

vim.cmd [[ set clipboard^=unnamedplus ]]

vim.opt.title = true

vim.cmd [[
    filetype plugin indent on
]]
vim.opt.hidden = true
vim.opt.scrolloff = 5

vim.opt.ignorecase = true
vim.opt.smartcase = true

local map = cfg_globals.map

map("n", "<Leader>cf", "<CMD>tcd ~/.config/nvim | Telescope find_files<CR>", {})
map("n", "<Leader>of", "<CMD>tcd ~/Dropbox/org | Telescope find_files<CR>", {})

vim.g.termdebug_wide = 1
vim.opt.showmode = false
vim.opt.laststatus = 2

vim.opt.inccommand = "nosplit"

vim.opt.foldenable = false
vim.opt.foldlevel = 20

vim.opt.number = true
vim.opt.ai = true

vim.opt.background = "dark"

vim.opt.cursorline = true
vim.opt.cursorlineopt = "number"

vim.opt.autoindent = true
vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.softtabstop = 0
vim.opt.shiftwidth = 4
vim.opt.linebreak = true

map("n", "<C-J>", "<C-W><C-J>", { noremap = true })
map("n", "<C-K>", "<C-W><C-K>", { noremap = true })
map("n", "<C-L>", "<C-W><C-L>", { noremap = true })
map("n", "<C-H>", "<C-W><C-H>", { noremap = true })

vim.cmd [[
    noremap <expr> j (v:count? 'j' : 'gj')
    noremap <expr> k (v:count? 'k' : 'gk')
]]

vim.g.BufstopAutoSpeedToggle = true

vim.opt.mouse = "a"
vim.opt.list = true
vim.opt.listchars = {
    tab = "  ",
    space = "·",
    trail = "∴",
    extends = ">",
    precedes = "<",
    eol = "↴",
}

vim.cmd [[hi NonText ctermfg=7 guifg=gray33]]

vim.opt.updatetime = 300

vim.opt.shortmess:append "c"

vim.opt.signcolumn = "yes"

map("n", "H", "gT", { noremap = true })
map("n", "L", "gt", { noremap = true })

vim.cmd [[
    augroup highlight_yank
        autocmd!
        au TextYankPost * silent! lua vim.highlight.on_yank { higroup='IncSearch', timeout=1000 }
    augroup END
]]

vim.cmd [[
    au BufReadPost *.asm set ft=65816
]]

vim.cmd [[
    set guifont=monospace:h13:#h-slight:#e-antialias
]]

vim.g.neovide_transparency = 0.9
vim.g.neovide_padding_bottom = 13
vim.g.neovide_padding_top = 13
vim.g.neovide_padding_left = 13
vim.g.neovide_padding_right = 13
vim.g.neovide_refresh_rate = 144
vim.g.neovide_scroll_animation_length = 0.5

vim.opt.pumblend = 15
vim.opt.winblend = 15

vim.cmd [[
    function! TermColors()
        let g:terminal_color_0 = "#434759"
        let g:terminal_color_1 = "#f07178"
        let g:terminal_color_2 = "#c3e88d"
        let g:terminal_color_3 = "#ffcb6b"
        let g:terminal_color_4 = "#82aaff"
        let g:terminal_color_5 = "#c792ea"
        let g:terminal_color_6 = "#89ddff"
        let g:terminal_color_7 = "#d0d0d0"
        let g:terminal_color_8 = "#434758"
        let g:terminal_color_9 = "#ff8b92"
        let g:terminal_color_10 = "#ddffa7"
        let g:terminal_color_11 = "#ffe585"
        let g:terminal_color_12 = "#9cc4ff"
        let g:terminal_color_13 = "#e1acff"
        let g:terminal_color_14 = "#a3f7ff"
        let g:terminal_color_15 = "#fefefe"
    endfunction
    autocmd TermOpen * call TermColors()
]]
