return {
    {
        "marko-cerovac/material.nvim",
        config = function()
            vim.cmd [[ syntax on ]]
            vim.g.material_style = "palenight"
            require("material").setup {
                disable = {
                    colored_cursor = true,
                    background = true,
                },
                styles = {
                    comments = { italic = true },
                },
                plugins = {
                    -- Available plugins:
                    "dap",
                    "dashboard",
                    "gitsigns",
                    "hop",
                    "indent-blankline",
                    "lspsaga",
                    "mini",
                    "neogit",
                    "nvim-cmp",
                    "nvim-navic",
                    "nvim-tree",
                    "nvim-web-devicons",
                    "sneak",
                    "telescope",
                    "trouble",
                    "which-key",
                },
                custom_highlights = {
                    LineNr = { fg = "#606488" },
                    CursorLineNr = { fg = "#C3C5D5" },
                    IndentBlanklineChar = { fg = "#4C4973" },
                    IndentBlanklineContextChar = { fg = "#959DCB" },
                    IndentBlanklineSpaceChar = { fg = "#4C4973" },
                    TreesitterContextLineNumber = { fg = "#C3E88D" },
                    Normal = { bg = "#292d3e", fg = "#959dcb" },
                },
            }
            vim.cmd [[ colorscheme material ]]
            vim.cmd [[ hi LineNr guifg=#606488 ]]
            vim.cmd [[ hi CursorLineNr guifg=#C3C5D5 ]]
        end,
    },
}
