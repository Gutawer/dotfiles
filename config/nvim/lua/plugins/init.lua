return {
    "tpope/vim-sleuth",
    {
        "gabrielelana/vim-markdown",
        config = function()
            vim.g.markdown_enable_spell_checking = 0
        end,
    },

    "tpope/vim-repeat",
    {
        "ggandor/leap.nvim",
        config = function()
            require("leap").set_default_keymaps()
            vim.cmd [[ autocmd User LightspeedLeave set scrolloff=5 ]]
        end,
    },

    {
        "lewis6991/gitsigns.nvim",
        config = function()
            require("gitsigns").setup()
        end,
    },

    {
        "mihaifm/bufstop",
        config = function()
            vim.cmd [[ :BufstopSpeedToggle ]]
        end,
    },

    "marrub--/vim-zscript",
    { "kaarmu/typst.vim", ft = "typst" },
    "elkowar/yuck.vim",
    "ARM9/snes-syntax-vim",
    "DingDean/wgsl.vim",
    "NoahTheDuke/vim-just",

    {
        "iamcco/markdown-preview.nvim",
        build = function()
            vim.fn["mkdp#util#install"]()
        end,
    },
    --{
    --    "lervag/vimtex",
    --    config = function()
    --        vim.g.vimtex_view_general_viewer = "okular"
    --        vim.g.vimtex_view_general_options = "--unique file:@pdf\\#src:@line@tex"
    --        vim.g.vimtex_matchparen_enabled = false
    --    end,
    --},

    {
        "goolord/alpha-nvim",
        config = function()
            local alpha = require "alpha"
            local dashboard = require "alpha.themes.dashboard"
            dashboard.section.header.val = {
                [[    _   __                _         ]],
                [[   / | / /__  ____ _   __(_)___ ___ ]],
                [[  /  |/ / _ \/ __ \ | / / / __ `__ \]],
                [[ / /|  /  __/ /_/ / |/ / / / / / / /]],
                [[/_/ |_/\___/\____/|___/_/_/ /_/ /_/ ]],
                [[                                    ]],
            }

            dashboard.section.buttons.val = {
                dashboard.button("e", "  New File", ":ene <BAR> startinsert <CR>"),
                dashboard.button("f", "  Find File", ":Telescope find_files<CR>"),
                dashboard.button("r", "  Recent Files", ":Telescope oldfiles<CR>"),
                dashboard.button(
                    "s",
                    "  Settings",
                    "<CMD>tcd ~/.config/nvim | Telescope find_files<CR>"
                ),
            }

            alpha.setup(dashboard.opts)
        end,
    },
    {
        "junegunn/vim-easy-align",
        config = function()
            local map = cfg_globals.map
            map("x", "ga", "<Plug>(EasyAlign)", {})
            map("n", "ga", "<Plug>(EasyAlign)", {})
        end,
    },
    "tpope/vim-surround",

    {
        "nvim-orgmode/orgmode",
        config = function()
            require("orgmode").setup_ts_grammar()

            require("orgmode").setup {
                org_agenda_files = { "~/Dropbox/org/*" },
                org_default_notes_file = "~/Dropbox/org/refile.org",
            }
        end,
    },
    {
        "akinsho/org-bullets.nvim",
        config = function()
            require("org-bullets").setup {
                symbols = { "◉", "○", "✸", "✿" },
            }
        end,
    },

    "nvim-lua/popup.nvim",
    "nvim-lua/plenary.nvim",

    {
        "kyazdani42/nvim-web-devicons",
        config = function()
            require("nvim-web-devicons").setup {
                default = true,
            }
        end,
    },
}
