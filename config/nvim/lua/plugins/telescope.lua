return {
    "nvim-telescope/telescope-rg.nvim",
    "nvim-telescope/telescope-ui-select.nvim",
    { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
    {
        "nvim-telescope/telescope.nvim",
        config = function()
            local default_picker_config = {
                theme = "ivy",
            }
            require("telescope").setup {
                pickers = {
                    find_files = default_picker_config,
                    live_grep = default_picker_config,
                    buffers = default_picker_config,
                    oldfiles = default_picker_config,
                    marks = default_picker_config,
                    colorscheme = default_picker_config,
                },
                extensions = {
                    fzf = {
                        fuzzy = true,
                        override_generic_sorter = true,
                        override_file_sorter = true,
                        case_mode = "smart_case",
                    },
                    ["ui-select"] = {
                        require("telescope.themes").get_dropdown {},
                    },
                },
                defaults = {
                    vimgrep_arguments = {
                        "rg",
                        "--color=never",
                        "--no-heading",
                        "--with-filename",
                        "--line-number",
                        "--column",
                        "--smart-case",
                        "-L",
                    },
                    winblend = 50,
                },
            }
            require("telescope").load_extension "fzf"
            require("telescope").load_extension "ui-select"

            local map = cfg_globals.map
            map(
                "n",
                "<Leader>z",
                '<CMD>lua require("telescope.builtin").find_files({ follow = true })<CR>',
                { noremap = true }
            )
            map(
                "n",
                "<Leader>ff",
                '<CMD>lua require("telescope.builtin").find_files({ follow = true })<CR>',
                { noremap = true }
            )
            map(
                "n",
                "<Leader>fg",
                '<CMD>lua require("telescope.builtin").live_grep()<CR>',
                { noremap = true }
            )
            map(
                "n",
                "<Leader>fb",
                '<CMD>lua require("telescope.builtin").buffers()<CR>',
                { noremap = true }
            )
        end,
    },
}
