return {
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-buffer",
    "dcampos/nvim-snippy",
    "dcampos/cmp-snippy",
    "hrsh7th/cmp-nvim-lsp-signature-help",
    "honza/vim-snippets",
    "ckunte/latex-snippets-vim",
    {
        "hrsh7th/nvim-cmp",
        config = function()
            local cmp = require "cmp"
            cmp.setup {
                snippet = {
                    expand = function(args)
                        -- vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
                        -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
                        -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
                        require("snippy").expand_snippet(args.body) -- For `snippy` users.
                    end,
                },
                mapping = cmp.mapping.preset.insert {
                    ["<C-d>"] = cmp.mapping.scroll_docs(-4),
                    ["<C-f>"] = cmp.mapping.scroll_docs(4),
                    ["<C-Space>"] = cmp.mapping.complete(),
                    ["<C-e>"] = cmp.mapping.close(),
                    ["<CR>"] = cmp.mapping.confirm { select = false },
                },
                sources = cmp.config.sources({
                    { name = "nvim_lsp" },
                    { name = "snippy" },
                    { name = "nvim_lsp_signature_help" },
                }, {
                    { name = "orgmode" },
                }, {
                    { name = "buffer" },
                }),
                experimental = {
                    ghost_text = {
                        hl_group = "Comment",
                    },
                },
                formatting = {
                    format = require("lspkind").cmp_format { with_text = true, maxwidth = 50 },
                },
            }

            vim.opt.completeopt = { "menuone", "noinsert", "noselect" }

            local map = cfg_globals.map

            local tab_action =
                "snippy#can_expand_or_advance() ? '<Plug>(snippy-expand-or-advance)' : '<Tab>'"
            map("i", "<Tab>", tab_action, { expr = true })
            map("s", "<Tab>", tab_action, { expr = true })

            local stab_action = "snippy#can_jump(-1) ? '<Plug>(snippy-previous)' : '<S-Tab>'"
            map("i", "<S-Tab>", stab_action, { expr = true })
            map("s", "<S-Tab>", stab_action, { expr = true })
        end,
    },
}
