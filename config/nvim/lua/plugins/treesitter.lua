return {
    "nvim-treesitter/playground",
    "IndianBoy42/tree-sitter-just",
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        config = function()
            require("tree-sitter-just").setup {}
            require("nvim-treesitter.configs").setup {
                ensure_installed = "all",
            }
        end,
    },
    {
        "romgrk/nvim-treesitter-context",
        config = function()
            require("treesitter-context").setup {
                patterns = {
                    default = {
                        "class",
                        "function_declaration",
                        "function_definition",
                        "function_item",
                        "method",
                        "interface",
                        "struct_item",
                        "enum",
                    },
                    lua = {},
                },
            }
        end,
    },
    {
        "lukas-reineke/indent-blankline.nvim",
        config = function()
            require("indent_blankline").setup {
                space_char_blankline = " ",
                show_end_of_line = true,
                show_current_context = true,
                context_patterns = {
                    "field_.*_list",
                    "enum_.*_list",
                    "^array",
                    "^tuple",
                    "^argument",
                    "^if",
                    ".*pattern.*",
                    "^for",
                    "^while",
                    "^parameter",
                    "impl_item",
                    "compound_statement",
                    "condition_clause",
                    "initializer_list",
                    "enumerator_list",
                    "^method",
                    "^function",
                    "^struct",
                    "^class",
                    "^match",
                    "^switch",
                    "call_expression",
                    "^try",
                    "^preproc",
                    "table",
                },
                buftype_exclude = {
                    "terminal",
                },
                filetype_exclude = {
                    "packer",
                    "help",
                    "startify",
                    "dashboard",
                    "lsp-installer",
                    "NvimTree",
                    "alpha",
                    "org",
                    "mason",
                    "lazy",
                    "",
                },
            }
        end,
    },
}
