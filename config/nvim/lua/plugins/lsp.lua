local globals = cfg_globals
local signs = globals.signs
local telescope = globals.telescope

return {
    {
        "williamboman/mason.nvim",
        config = function()
            require("mason").setup()
        end,
    },
    "jose-elias-alvarez/null-ls.nvim",
    {
        "jay-babu/mason-null-ls.nvim",
        config = function()
            require("mason-null-ls").setup {
                ensure_installed = {
                    "stylua",
                    "black",
                    "prettierd",
                    "rustfmt",
                    "latexindent",
                },
            }
        end,
    },
    {
        "williamboman/mason-lspconfig.nvim",
        config = function()
            require("mason-lspconfig").setup {
                ensure_installed = {
                    "rust_analyzer",
                    "pyright",
                    "lua_ls",
                    "tsserver",
                    "texlab",
                },
            }
        end,
    },

    "simrat39/rust-tools.nvim",
    "vigoux/ltex-ls.nvim",
    "onsails/lspkind-nvim",
    "kosayoda/nvim-lightbulb",
    "RRethy/vim-illuminate",
    "j-hui/fidget.nvim",
    "folke/lsp-colors.nvim",
    {
        "folke/trouble.nvim",
        config = function()
            for type, icon in pairs(signs) do
                local hl = "DiagnosticSign" .. type
                vim.fn.sign_define(hl, { text = icon .. " ", texthl = hl, numhl = hl })
            end
            require("trouble").setup {
                signs = {
                    error = signs.Error,
                    warning = signs.Warn,
                    hint = signs.Hint,
                    information = signs.Info,
                    other = "",
                },
            }
        end,
    },

    {
        "abzcoding/renamer.nvim",
        branch = "develop",
        config = function()
            require("renamer").setup {
                -- The popup title, shown if `border` is true
                title = "Rename",
                -- The padding around the popup content
                padding = {
                    top = 0,
                    left = 0,
                    bottom = 0,
                    right = 0,
                },
                -- Whether or not to shown a border around the popup
                border = true,
                -- The characters which make up the border
                border_chars = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
                -- Whether or not to highlight the current word references through LSP
                show_refs = true,
            }
        end,
    },
    {
        "neovim/nvim-lspconfig",
        config = function()
            local capabilities = require("cmp_nvim_lsp").default_capabilities()
            _G.cfg_globals.capabilities = capabilities

            local on_attach = function(client)
                local map = cfg_globals.map

                client.server_capabilities.document_formatting = false
                client.server_capabilities.document_range_formatting = false

                require("illuminate").on_attach(client)

                -- Mappings.
                local opts = { noremap = true, silent = true }

                -- See `:help vim.lsp.*` for documentation on any of the below functions
                map("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
                map("n", "gd", telescope "lsp_definitions", opts)
                map("n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
                map("n", "gi", telescope "lsp_implementations", opts)
                map("n", "<leader>wa", "<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>", opts)
                map("n", "<leader>wr", "<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>", opts)
                map(
                    "n",
                    "<leader>wl",
                    "<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>",
                    opts
                )
                map("n", "<leader>D", telescope "lsp_type_definitions", opts)
                map("n", "<leader>rn", "<cmd>lua require('renamer').rename()<CR>", opts)
                map("n", "<leader>ca", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
                map("n", "gr", telescope "lsp_references", opts)
                map(
                    "n",
                    "<leader>e",
                    '<cmd>lua vim.diagnostic.open_float(0, { scope = "cursor" })<CR>',
                    opts
                )
                map(
                    "n",
                    "[d",
                    "<cmd>lua vim.diagnostic.goto_prev({ focusable = false, float = false })<CR>",
                    opts
                )
                map(
                    "n",
                    "]d",
                    "<cmd>lua vim.diagnostic.goto_next({ focusable = false, float = false })<CR>",
                    opts
                )
                map("n", "<leader>q", "<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>", opts)
                map("n", "<leader>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)
            end
            _G.cfg_globals.on_attach = on_attach

            local rust_opts = {
                tools = {
                    -- rust-tools options
                    -- Automatically set inlay hints (type hints)
                    autoSetHints = true,

                    runnables = {
                        -- whether to use telescope for selection menu or not
                        use_telescope = true,

                        -- rest of the opts are forwarded to telescope
                    },

                    debuggables = {
                        -- whether to use telescope for selection menu or not
                        use_telescope = true,

                        -- rest of the opts are forwarded to telescope
                    },

                    -- These apply to the default RustSetInlayHints command
                    inlay_hints = {
                        -- Only show inlay hints for the current line
                        only_current_line = false,

                        -- Event which triggers a refersh of the inlay hints.
                        -- You can make this "CursorMoved" or "CursorMoved,CursorMovedI" but
                        -- not that this may cause  higher CPU usage.
                        -- This option is only respected when only_current_line and
                        -- autoSetHints both are true.
                        only_current_line_autocmd = "CursorHold",

                        -- wheter to show parameter hints with the inlay hints or not
                        show_parameter_hints = true,

                        -- prefix for parameter hints
                        parameter_hints_prefix = "<- ",

                        -- prefix for all the other hints (type, chaining)
                        other_hints_prefix = "=> ",

                        -- whether to align to the length of the longest line in the file
                        max_len_align = false,

                        -- padding from the left if max_len_align is true
                        max_len_align_padding = 1,

                        -- whether to align to the extreme right or not
                        right_align = false,

                        -- padding from the right if right_align is true
                        right_align_padding = 7,

                        -- The color of the hints
                        highlight = "Comment",
                    },

                    hover_actions = {
                        -- whether the hover action window gets automatically focused
                        auto_focus = false,
                        border = "none",
                    },

                    -- settings for showing the crate graph based on graphviz and the dot
                    -- command
                    crate_graph = {
                        -- Backend used for displaying the graph
                        -- see: https://graphviz.org/docs/outputs/
                        -- default: x11
                        backend = "x11",
                        -- where to store the output, nil for no output stored (relative
                        -- path from pwd)
                        -- default: nil
                        output = nil,
                        -- true for all crates.io and external crates, false only the local
                        -- crates
                        -- default: true
                        full = true,
                    },
                },

                -- all the opts to send to nvim-lspconfig
                -- these override the defaults set by rust-tools.nvim
                -- see https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#rust_analyzer
                server = {
                    on_attach = function(client)
                        on_attach(client)
                        local opts = { noremap = true, silent = true }
                        cfg_globals.map("n", "K", "<cmd>:RustHoverActions<CR>", opts)
                    end,
                    capabilities = capabilities,
                    settings = {
                        -- to enable rust-analyzer settings visit:
                        -- https://github.com/rust-analyzer/rust-analyzer/blob/master/docs/user/generated_config.adoc
                        ["rust-analyzer"] = {
                            -- enable clippy on save
                            checkOnSave = {
                                command = "clippy",
                            },
                            diagnostics = {
                                disabled = {
                                    "unresolved-macro-call",
                                },
                            },
                            inlayHints = { locationLinks = false },
                        },
                    },
                }, -- rust-analyzer options
            }

            require("rust-tools").setup(rust_opts)

            local runtime_path = vim.split(package.path, ";")
            table.insert(runtime_path, "lua/?.lua")
            table.insert(runtime_path, "lua/?/init.lua")

            vim.diagnostic.config {
                severity_sort = true,
                float = { border = "none" },
            }

            vim.cmd [[
                autocmd CursorHold,CursorHoldI * lua require("nvim-lightbulb").update_lightbulb()
            ]]

            vim.fn.sign_define("LightBulbSign", {
                text = " ",
                texthl = "LightBulbSign",
                linehl = "LightBulbSign",
                numhl = "LightBulbSign",
            })

            vim.cmd [[
                autocmd CursorHold * lua vim.diagnostic.open_float(0, { focusable = false, scope = "cursor" })
            ]]

            require("fidget").setup {
                text = {
                    spinner = "dots",
                },
                sources = {
                    ltex = {
                        ignore = true,
                    },
                },
            }

            local lspconfig = require "lspconfig"
            lspconfig.lua_ls.setup {
                on_attach = on_attach,
                capabilities = capabilities,
                settings = {
                    Lua = {
                        runtime = {
                            version = "LuaJIT",
                            -- Setup your lua path
                            path = runtime_path,
                        },
                        diagnostics = {
                            globals = { "vim" },
                        },
                        workspace = {
                            library = vim.api.nvim_get_runtime_file("", true),
                            checkThirdParty = false,
                        },
                        telemetry = {
                            enable = false,
                        },
                    },
                },
            }
            lspconfig.pyright.setup {
                on_attach = on_attach,
                capabilities = capabilities,
            }
            lspconfig.tsserver.setup {
                on_attach = on_attach,
                capabilities = capabilities,
            }
            -- lspconfig.typst_lsp.setup {
            --     on_attach = on_attach,
            --     capabilities = capabilities,
            -- }
            require("ltex-ls").setup {
                on_attach = on_attach,
                capabilities = capabilities,
                use_spellfile = false,
                window_border = "none",
                filetypes = { "latex", "tex", "bib", "markdown" },
                settings = {
                    ltex = {
                        enabled = { "latex", "tex", "bib", "markdown" },
                        language = "auto",
                        diagnosticSeverity = "information",
                        sentenceCacheSize = 2000,
                        additionalRules = {
                            enablePickyRules = true,
                            motherTongue = "en-GB",
                        },
                        disabledRules = {},
                        dictionary = (function()
                            -- For dictionary, search for files in the runtime to have
                            -- and include them as externals the format for them is
                            -- dict/{LANG}.txt
                            --
                            -- Also add dict/default.txt to all of them
                            local files = {}
                            for _, file in ipairs(vim.api.nvim_get_runtime_file("dict/*", true)) do
                                local lang = vim.fn.fnamemodify(file, ":t:r")
                                local fullpath = vim.fs.normalize(file, ":p")
                                files[lang] = { ":" .. fullpath }
                            end

                            if files.default then
                                for lang, _ in pairs(files) do
                                    if lang ~= "default" then
                                        vim.list_extend(files[lang], files.default)
                                    end
                                end
                                files.default = nil
                            end
                            return files
                        end)(),
                    },
                },
            }
            lspconfig.texlab.setup {
                on_attach = on_attach,
                capabilities = capabilities,
                settings = {
                    texlab = {
                        build = {
                            executable = "latexmk",
                            args = {
                                "%f",
                            },
                            onSave = true,
                        },
                    },
                },
            }

            local null_ls = require "null-ls"
            null_ls.setup {
                sources = {
                    null_ls.builtins.formatting.stylua,
                    null_ls.builtins.formatting.rustfmt,
                    null_ls.builtins.formatting.prettierd,
                    null_ls.builtins.formatting.black,
                    null_ls.builtins.formatting.latexindent,
                    null_ls.builtins.diagnostics.pylint,
                },
            }
            vim.cmd [[
                autocmd BufWritePre * lua vim.lsp.buf.format()
            ]]
        end,
    },
}
