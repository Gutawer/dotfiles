return {
    {
        "nvim-lualine/lualine.nvim",
        dependencies = {
            "marko-cerovac/material.nvim",
        },
        config = function()
            local signs = cfg_globals.signs
            require("lualine").setup {
                options = {
                    icons_enabled = true,
                    theme = require "lualine.themes.palenight",
                    component_separators = { left = "", right = "" },
                    section_separators = { left = "", right = "" },
                    disabled_filetypes = {},
                    always_divide_middle = true,
                },
                sections = {
                    lualine_a = { "mode" },
                    lualine_b = {
                        "branch",
                        "diff",
                        {
                            "diagnostics",
                            sources = { "nvim_diagnostic" },
                            sections = { "error", "warn", "info", "hint" },
                            symbols = {
                                error = signs.Error .. " ",
                                warn = signs.Warn .. " ",
                                info = signs.Info .. " ",
                                hint = signs.Hint .. " ",
                            },
                        },
                        'string.gsub(require"lsp-status".status(), "^%s*(.-)%s*$", "%1")',
                    },
                    lualine_c = {
                        { "filename", path = 0 },
                    },
                    lualine_x = { "encoding", "fileformat", "filetype" },
                    lualine_y = { "progress" },
                    lualine_z = { "location" },
                },
                inactive_sections = {
                    lualine_a = {},
                    lualine_b = {},
                    lualine_c = { "filename" },
                    lualine_x = { "location" },
                    lualine_y = {},
                    lualine_z = {},
                },
                tabline = {
                    lualine_a = {
                        {
                            "tabs",
                            max_length = vim.o.columns / 3, -- maximum width of tabs component
                            mode = 0,
                            tabs_color = {
                                active = "lualine_a_normal", -- color for active tab
                                inactive = "lualine_b_normal", -- color for inactive tab
                            },
                        },
                    },
                    lualine_b = {},
                    lualine_c = {},
                    lualine_x = {
                        { "filename", path = 1 },
                    },
                    lualine_y = {},
                    lualine_z = {},
                },
                extensions = {},
            }
        end,
    },
}
