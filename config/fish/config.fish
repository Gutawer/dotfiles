if status is-interactive
    # Commands to run in interactive sessions can go here
end

set -gx EDITOR nvim

set fish_cursor_default     block      blink
set fish_cursor_insert      line       blink
set fish_cursor_replace_one underscore blink
set fish_cursor_visual      block

zoxide init fish | source
thefuck --alias | source
