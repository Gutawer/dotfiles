function bobthefish_colors --description 'Define a custom bobthefish color scheme' --no-scope-shadowing

    # then override everything you want! note that these must be defined with `set -x`
    set -x color_initial_segment_exit c792ea 3e4452 --bold
    set -x color_initial_segment_private c792ea 3e4452
    set -x color_initial_segment_su c792ea 3e4452 --bold
    set -x color_initial_segment_jobs c792ea 3e4452 --bold
    set -x color_path 3e4452 c792ea
    set -x color_path_basename 3e4452 c792ea --bold
    set -x color_path_nowrite 660000 cc9999
    set -x color_path_nowrite_basename 660000 cc9999 --bold
    set -x color_repo 3e4452 c792ea
    set -x color_repo_work_tree 3e4452 c792ea --bold
    set -x color_repo_dirty 3e4452 c792ea
    set -x color_repo_staged 3e4452 c792ea
    set -x color_vi_mode_default c792ea 333333 --bold
    set -x color_vi_mode_insert c792ea 333333 --bold
    set -x color_vi_mode_visual c792ea 3a2a03 --bold
    set -x color_vagrant 48b4fb ffffff --bold
    set -x color_aws_vault
    set -x color_aws_vault_expired
    set -x color_username cccccc 255e87 --bold
    set -x color_hostname cccccc 255e87
    set -x color_rvm af0000 cccccc --bold
    set -x color_virtualfish 005faf cccccc --bold
    set -x color_virtualgo 005faf cccccc --bold
    set -x color_desk 005faf cccccc --bold
    set -x color_nix 005faf cccccc --bold
end
