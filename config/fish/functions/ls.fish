function ls --wraps='exa -al --icons --group-directories-first' --description 'alias ls=exa -al --icons --group-directories-first'
  exa -al --icons --group-directories-first $argv; 
end
